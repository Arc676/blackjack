CC=gcc

blackjack:
	test -s target/debug/libblackjack.a || cargo build
	mkdir -p target/c
	$(CC) csrc/blackjack.c -I include -L target/debug -l blackjack -o target/c/blackjack
